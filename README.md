# Any Subtitle

Any Subtitle is a project that enables the user to watch a movie in a theater with a subtitle of any language on his device as assistance.

  - The user places his mobile handset on the holder on the seat in front of him
  - Selects the movie and session time
  - Enjoys the synchronised subtitle of his choice

### Todos

 - Decide the technology stack
 - Build and Test

License
----

The License model is yet to be decided.
